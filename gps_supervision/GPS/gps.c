/*
 * gps.c
 * 		GPS for Solar Boat Team
 *		ver. 1
 * 		Created on: 25.02.2019
 *      Author: Rafal Radomski
 */
#include "gps.h"
//#include "../stm32f4xx_hal.h"
//#include "minmea.h"
//#include "uartRxMachinery.h"

extern void uartRxMachinery_ResetBuffer();
extern struct uartRxMachinery uartRxMachinery;

uint8_t UBXConfigSetTab[] = {
		  // Rate
//		  0xB5,0x62,0x06,0x08,0x06,0x00,0x64,0x00,0x01,0x00,0x01,0x00,0x7A,0x12, //(10Hz)
//		  0xB5,0x62,0x06,0x08,0x06,0x00,0xC8,0x00,0x01,0x00,0x01,0x00,0xDE,0x6A, //(5Hz)
		  0xB5,0x62,0x06,0x08,0x06,0x00,0xF4,0x01,0x01,0x00,0x01,0x00,0x0B,0x77, //(2Hz)
//		  0xB5,0x62,0x06,0x08,0x06,0x00,0xE8,0x03,0x01,0x00,0x01,0x00,0x01,0x39, //(1Hz)
//		  0xB5,0x62,0x06,0x08,0x06,0x00,0xD0,0x07,0x01,0x00,0x01,0x00,0xED,0xBD, //(0,5Hz)
};

/****************************************************************************
 *
 *			FUNCTION SUPPORTNG ONE LINE (NMEA FRAME)
 *
 * @brief
 * 	Read one line of NMEA snestences from GPS and extracting data from this senstences
 *
 * @argumets
 *	incomingLine - pointer to table witch one line NMEA code ( uartRxMachinery.uartRxBuffer from uartRxMachinery.c)
 *
 ****************************************************************************/
void uartLineCb(uint8_t* incomingLine)
{
	 myGPS = minmea_sentence_id((const char * )incomingLine,false);
	 switch(myGPS)
	 		{
	 /***** MINMEA_SENTENCE_GGA ********/
	 			case MINMEA_SENTENCE_GGA:
	 			{
	 				if (!minmea_parse_gga(&frame_gga, (const char * )incomingLine))
					{
	 					GPS_Error();
					}

	 			}break;

	 /***** MINMEA_SENTENCE_RMC ********/
	 			 case MINMEA_SENTENCE_RMC:
	 			 {
					if (!minmea_parse_rmc(&frame_rmc, (const char * )incomingLine))
					{
						GPS_Error();
					}
	 			 }break;

		/***** MINMEA_INVALID ********/
	 			case MINMEA_INVALID:
	 			{
	 				GPS_Error();
	 			} break;

		/***** Default ********/
	 			 default:
	 				GPS_Error();
	 				break;
	 		}/*swich*/

}

/****************************************************************************
 *
 *			Init function
 *
 * @brief
 * 	Inicjalization od GPS
 *
 * @argumets
 *	poiter to  huart  struct thos depend of GPS
 *
 ****************************************************************************/
void GPS_Init(UART_HandleTypeDef *huart)
{
	huart_gps=huart;
	is_gps_redirect_mod_enabld = false;
	uartRxMachinery_Init(huart);
	uartRxMachinery_RegisterLineCallback(uartLineCb);
}

/****************************************************************************
 *
 *			GPS_configure_sendingAlsoDataFromGpsToOtherDest
 * @brief
 * 	redirect Recivet data from GPS in NMEA code to UART
 *
 * @argumets
 *	huartDestiny: pointer to huart to which we want to send data from GPS also
 *
 ****************************************************************************/
void GPS_configure_sendingAlsoDataFromGpsToOtherDest(UART_HandleTypeDef *huartDestiny)
{
  if(is_gps_redirect_mod_enabld || huart_gps != huartDestiny /*|| dataToSendPtr != 0*/){
	  huart_to_send_gps_data_also = huartDestiny; //set ptr to UART handler
  }
}

/****************************************************************************
 *
 *			GPS_enable_sendingAlsoDataFromGpsToOtherDest
 * @brief
 * 	enable ending Recivet data from GPS in NMEA code to other UART also
 *
 ****************************************************************************/
void GPS_enable_sendingAlsoDataFromGpsToOtherDest(){
	if(is_gps_redirect_mod_enabld) /*is_gps_redirect_mod_enabld is already set*/ GPS_Error();
	else if(huart_to_send_gps_data_also == (UART_HandleTypeDef*)0 ) /*huartDestiny is not set*/GPS_Error();
	else if(huart_gps == huart_to_send_gps_data_also) /*huartGps and huartDestiny is the same*/GPS_Error();
	else {
		is_gps_redirect_mod_enabld = 1; //set is_gps_redirect_mod_enabld
	}
}

/****************************************************************************
 *
 *			GPS_disable_sendingAlsoDataFromGpsToOtherDest
 * @brief
 * 	disable sending Recivet data from GPS in NMEA code to other UART also
 *
 ****************************************************************************/
void GPS_disable_sendingAlsoDataFromGpsToOtherDest(){
	is_gps_redirect_mod_enabld = false;
}



/****************************************************************************
 *
 *			GPS_sendConfigurationToGps
 * @brief
 * 	to sent configuration data do GPS thru UART in UBX standard
 *
 ****************************************************************************/
void GPS_sendConfigurationToGps(){
#ifdef UART_GPS_USE_DMA
	HAL_UART_Transmit_DMA(huart_gps, UBXConfigSetTab, sizeof(UBXConfigSetTab)/sizeof(uint8_t));
#else
	HAL_UART_Transmit_IT(huart_gps, UBXConfigSetTab, sizeof(UBXConfigSetTab)/sizeof(uint8_t));
#endif
	HAL_Delay(5);
}


/****************************************************************************
 *
 *			IN HAL_UART_RxCpltCallback Uses
 * @brief
 * 	to redirect data to library in HAL__UART_RxCpltCallback interrupt
 *
 * @argumets
 *	poiter to  huart  struct thos depend of GPS
 *
 ****************************************************************************/
void GPS_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{

	if(huart == huart_gps){ 		//Is it data from UART connected to GPS?

		if(is_gps_redirect_mod_enabld || huart_to_send_gps_data_also != 0)
#ifdef UART_GPS_USE_DMA
			HAL_UART_Transmit_DMA(/*&*/huart_to_send_gps_data_also,&uartRxMachinery.uartRxChar, 1);
#else
			HAL_UART_Transmit_IT(/*&*/huart_to_send_gps_data_also,&uartRxMachinery.uartRxChar, 1);
#endif
	uart_RxMachinery(huart);
	}
}

/****************************************************************************
 *
 *			GPS_get_latitude_float/cord
 *
 * @brief
 * 	to get latitude
 *
 * @return
 *	_float: retunr raw data in floating point type
 *	_cord: return decimal degrees (DD) in floating point type
 *
 ****************************************************************************/
float GPS_get_latitude_float()
{
//	if (frame_gga.latitude.value != 0)
//		return minmea_tofloat(&frame_gga.latitude);
	return minmea_tofloat(&frame_rmc.latitude);
}

float GPS_get_latitude_cord()
{
//	if (frame_gga.latitude.value != 0)
//			return minmea_tocoord(&frame_gga.latitude);
		return minmea_tocoord(&frame_rmc.latitude);
}

/****************************************************************************
 *
 *			GPS_get_longitude_float/cord
 *
 * @brief
 * 	to get longitude
 *
 * @return
 *	_float: retunr raw data in floating point type
 *	_cord: return decimal degrees (DD) in floating point type
 *
 ****************************************************************************/
float GPS_get_longitude_float()
{
//	if (frame_gga.longitude.value != 0)
//			return minmea_tofloat(&frame_gga.longitude);
		return minmea_tofloat(&frame_rmc.longitude);
}

float GPS_get_longitude_cord()
{

//	if (frame_gga.longitude.value != 0)
//				return minmea_tocoord(&frame_gga.longitude);
			return minmea_tocoord(&frame_rmc.longitude);
}

/****************************************************************************
 *
 *			GPS_get_speed_float/cord
 * @brief
 * 	to get speed
 *
 * @return
 *	_float: retunr raw data in floating point type
 *	_cord: return decimal degrees (DD) in floating point type
 *
 ****************************************************************************/
float GPS_get_speed_float()
{
	return minmea_tofloat(&frame_rmc.speed);
}

/****************************************************************************
 *
 *			GPS_get_timestamp
 *
 * @brief
 * 	to get timestamp from GPS
 *
 * @return
 *	timestamp
 *
 ****************************************************************************/
time_t GPS_get_timestamp()
{
	if(frame_rmc.date.year != 0)
	minmea_gettime(&timespec,&frame_rmc.date,&frame_rmc.time);
	return timespec.tv_sec;
}

/****************************************************************************
 *
 *			GPS_get_isFixed
 *
 * @brief
 *  showing it is GPS get fix (know where it is)
 *
 * @return
 *	timestamp
 *
 ****************************************************************************/
bool GPS_get_isFixed()
{
//	if ((frame_gga.latitude.value == 0 ) || (frame_gga.longitude.value == 0)) return false;
	if (frame_gga.latitude.value) return frame_gga.latitude.value;
	return frame_rmc.valid;
}

/****************************************************************************
 *
 *			GPS_Reset
 *
 ****************************************************************************/
void GPS_Reset()
{
	uartRxMachinery_ResetBuffer();
}

/****************************************************************************
 *
 *			GPS_Error
 *
 ****************************************************************************/
void GPS_Error()
{

}
