/*
 * uartRxMachinery.h
 *
 *  Created on: May 29, 2018
 *      Author: kacper
 */

#ifndef UARTRXMACHINERY_H_
#define UARTRXMACHINERY_H_

#include "stm32l4xx_hal.h"

//#include "utility.h"

struct uartRxMachinery {
	UART_HandleTypeDef* huart;
	uint8_t uartRxChar;
	uint8_t uartRxPointer;
	uint8_t uartRxBuffer[320];
	void (*uartRxLineCallback)(uint8_t*);
};

void uartRxMachinery_Init(UART_HandleTypeDef *huart);
void uartRxMachinery_RegisterLineCallback(void (*callback)(uint8_t*));
void uart_RxMachinery(UART_HandleTypeDef *huart);

#endif /* UARTRXMACHINERY_H_ */
