/*
 * uartRxMachinery.c
 *
 *  Created on: May 29, 2018
 *      Author:
 */

#include "uartRxMachinery.h"
#include "gps.h"

#include "../stm32l4xx_hal.h"

struct uartRxMachinery uartRxMachinery;

void uartRxMachinery_ResetBuffer();

void uartRxMachinery_Init(UART_HandleTypeDef *huart) {
	uartRxMachinery.huart = huart;
	uartRxMachinery.uartRxLineCallback = NULL;
	uartRxMachinery_ResetBuffer();
#ifdef UART_GPS_USE_DMA
	HAL_UART_Receive_DMA(huart, &uartRxMachinery.uartRxChar, 1);
#else
	HAL_UART_Receive_IT(huart, &uartRxMachinery.uartRxChar, 1);
#endif
}

void uartRxMachinery_RegisterLineCallback(void (*callback)(uint8_t*)) {
	uartRxMachinery.uartRxLineCallback = callback;
}

void uartRxMachinery_ResetBuffer() {
	uartRxMachinery.uartRxPointer = 0;
//	HAL_UART_Transmit_DMA(uartRxMachinery.huart, (uint8_t*) "\r\n", 4);

}

void uart_RxMachinery(UART_HandleTypeDef *huart) {
	if (uartRxMachinery.uartRxChar == '\r') {
//		uartRxMachinery.uartRxBuffer[uartRxMachinery.uartRxPointer++] = '\n'; // ---
//		uartRxMachinery.uartRxBuffer[uartRxMachinery.uartRxPointer++] = '\r'; // ---
		uartRxMachinery.uartRxBuffer[uartRxMachinery.uartRxPointer++] = '\0'; // set end
//				HAL_UART_Transmit_IT(huart,(uint8_t*) &(uartRxMachinery.uartRxChar), 1);
		// run callback if exist
		if (uartRxMachinery.uartRxLineCallback) {
			uartRxMachinery.uartRxLineCallback(uartRxMachinery.uartRxBuffer);
		}
		uartRxMachinery_ResetBuffer();	// reset
	} else if (uartRxMachinery.uartRxChar == '\n') { // ignore '\n'
	} else {
//		HAL_UART_Transmit_DMA(uartRxMachinery.huart,
//				(uint8_t*) &uartRxMachinery.uartRxChar, 1);
//		HAL_UART_Transmit_IT(huart,(uint8_t*) &(uartRxMachinery.uartRxChar), 1);
		uartRxMachinery.uartRxBuffer[uartRxMachinery.uartRxPointer++] =
				uartRxMachinery.uartRxChar;
	}

#ifdef UART_GPS_USE_DMA
	HAL_UART_Receive_DMA(huart, &uartRxMachinery.uartRxChar, 1);
#else
	HAL_UART_Receive_IT(huart, &uartRxMachinery.uartRxChar, 1);
#endif
}
