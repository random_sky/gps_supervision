/*
 * gps.h
 * 		GPS for Solar Boat Team
 *		ver. 1
 * 		Created on: 25.02.2019
 *      Author: Rafal Radomski
 */

#ifndef GPS_H_
#define GPS_H_

/****************************************************************************
 *
 *			INCLUDES
 *
 ****************************************************************************/
#include "stm32l4xx_hal.h"
#include "minmea.h"
#include "uartRxMachinery.h"

/****************************************************************************
 *
 *			DEFINES
 *
 ****************************************************************************/
#define	UART_GPS_USE_DMA

/****************************************************************************
 *
 *			DATA
 *
 ****************************************************************************/
enum minmea_sentence_id myGPS;
UART_HandleTypeDef *huart_gps;
struct timespec timespec;

struct minmea_sentence_gga frame_gga;
struct minmea_sentence_rmc frame_rmc;

uint8_t 				is_gps_redirect_mod_enabld;
UART_HandleTypeDef* 	huart_to_send_gps_data_also;
//uint8_t*				dataFromGpsToSend;



/****************************************************************************
 *
 *		 	FUNCTIONS
 *
 ****************************************************************************/

void GPS_Init(UART_HandleTypeDef *huart);

void GPS_configure_sendingAlsoDataFromGpsToOtherDest(UART_HandleTypeDef *huartDestiny);
void GPS_enable_sendingAlsoDataFromGpsToOtherDest();
void GPS_disable_sendingAlsoDataFromGpsToOtherDest();

void GPS_sendConfigurationToGps();

void uartLineCb(uint8_t* incomingLine);
void GPS_UART_RxCpltCallback(UART_HandleTypeDef *huart);

bool GPS_get_isFixed();

float GPS_get_latitude_float();
float GPS_get_latitude_cord();

float GPS_get_longitude_float();
float GPS_get_longitude_cord();

float GPS_get_speed_float();

time_t GPS_get_timestamp();

void GPS_Reset();
void GPS_Error(); //empty func



#endif /* GPS_H_ */
