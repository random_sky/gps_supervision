/*
 * userMain.h
 *
 *  Created on: 23.08.2020
 *      Author: asus
 */

#ifndef USERMAIN_H_
#define USERMAIN_H_

/****************************************************************************
 *
 *			INCLUDES
 *
 ****************************************************************************/
#include "stm32l4xx_hal.h"
#include "main.h"
#include "../GPS/gps.h"

/****************************************************************************
 *
 *			DEFINES
 *
 ****************************************************************************/
//Main settings of gps supervisor
#define MAX_DISTANCE_FROM_HOME 50
#define KM_TO_M 0.01
#define AMOUNT_OF_CORDS_TO_MV 5

//UART
#define HUART_GPS huart1
#define HUART_PC huart2

//LED
#define LED_GREEN_ON HAL_GPIO_WritePin(LED_GREEN_GPIO_Port,LED_GREEN_Pin,GPIO_PIN_RESET)
#define LED_GREEN_OFF HAL_GPIO_WritePin(LED_GREEN_GPIO_Port,LED_GREEN_Pin,GPIO_PIN_SET)
#define LED_GREEN_TOG HAL_GPIO_TogglePin(LED_GREEN_GPIO_Port,LED_GREEN_Pin)

#define LED_RED_ON HAL_GPIO_WritePin(LED_RED_GPIO_Port,LED_RED_Pin,GPIO_PIN_RESET)
#define LED_RED_OFF HAL_GPIO_WritePin(LED_RED_GPIO_Port,LED_RED_Pin,GPIO_PIN_SET)
#define LED_RED_TOG HAL_GPIO_TogglePin(LED_RED_GPIO_Port,LED_RED_Pin)

#define LED3_TOG HAL_GPIO_TogglePin(LED3_GPIO_Port,LED3_Pin)

/****************************************************************************
 *
 *			DATA
 *
 ****************************************************************************/
/*** Extern resources ***/
//UART's
extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;

//Timers
extern TIM_HandleTypeDef htim6;

//GPS machinery
extern struct uartRxMachinery uartRxMachinery;


/*** Data types ***/
//Possible return value
typedef enum{
    RETURN_OK = 0,
    RETURN_ERROR
 }EReturnCode;

 //States of state machine
 typedef enum{
     STATE_ERROR = -1,
	 STATE_INIT = 0,
	 STATE_HMI,
	 STATE_GPS,
	 STATE_CALC,
	 STATE_PRINT,
	 STATE_ALARM		//NOT USED STATE
  }EStateOfMachine;

 //Basics struct to handle coordinates of one point
 typedef struct{
	float latitude; 	//PL: szerokosc (=)
	float longitude; 	//PL: d�ugosc (||)
}SCoord; //TODO: it should be "coordinates"


/*** Global Variables ***/
//Current state machine
EStateOfMachine machineState;

//Coordinates
SCoord homeCoordinates = {51.133727, 16.974597}; //TODO: maybe some other init val
SCoord currentCoordinates;
static SCoord lastCordsTab[AMOUNT_OF_CORDS_TO_MV];
static uint8_t lastCordsTab_idx;
double distanceFromHome;

//Data to print
uint8_t dataToPrint[500];

//Event states
bool buttonEvent;
bool isAlarm;

/****************************************************************************
 *
 *		 	FUNCTIONS
 *
 ****************************************************************************/
//Main funcion - to separate *.[ch] file from those genereted by HAL
void userMain();


//Convert degrees to meter
//uint32_t LatitudeToMeter (float latitude);
//uint32_t LongitudeToMeter (float latitude);

//Calculate destance betwen two cords in meter
EReturnCode getDistanceBetweenTwoCoords(double 	*distInMeter,//TODO: getDistanceBetweenTwoCoords
										SCoord 	*pointA,
										SCoord 	*pointB);

//Printing info in UART (PC/BLuteooth) to debug
EReturnCode printInfo(UART_HandleTypeDef *huart, uint8_t *pData);

//Seting and clearing alarm
EReturnCode setAlarm();
EReturnCode cancelAlarm();

//Error handling
void errorOccurred();


/****************************************************************************
 *
 *		 	FOR TESTING
 *
 ****************************************************************************/
//Defines for testing
//#define FAKE_GPS_TEST

/********* FOR TEST *********/
//only for testing
//SCord testCordsTab[AMOUNT_OF_CORDS_TO_MV] ={{50.000000, 15.000000},
//											{51.000000, 16.000000},
//											{60.000000, 20.000000}};

//point A - HOME point
#define COORD_A {51.133727, 16.974597}
//	$GPGGA,213905.983,5108.024,N,01658.476,E,1,12,1.0,0.0,M,0.0,M,,*65
//	$GPGSA,A,3,01,02,03,04,05,06,07,08,09,10,11,12,1.0,1.0,1.0*30
//	$GPRMC,213905.983,A,5108.024,N,01658.476,E,,,020920,000.0,W*78

//point B - In Circle - 19.36m  from HOME point
//Latitude:	51.13387473�
//Longitude:	16.97474599�
#define COORD_B {51.133874, 16.974745}
//	$GPGGA,213906.983,5108.032,N,01658.485,E,1,12,1.0,0.0,M,0.0,M,,*6D
//	$GPGSA,A,3,01,02,03,04,05,06,07,08,09,10,11,12,1.0,1.0,1.0*30
//	$GPRMC,213906.983,A,5108.032,N,01658.485,E,055.0,043.7,020920,000.0,W*70

//point C - On The Border - 47,65m  from HOME point; (28.29m from In Circle poin)
//Latitude:	51.13409269�
//Longitude:	16.97495520�
#define COORD_C {51.1340926,16.974955}
//	$GPGGA,213907.983,5108.046,N,01658.497,E,1,12,1.0,0.0,M,0.0,M,,*6C
//	$GPGSA,A,3,01,02,03,04,05,06,07,08,09,10,11,12,1.0,1.0,1.0*30
//	$GPRMC,213907.983,A,5108.046,N,01658.497,E,029.2,045.6,020920,000.0,W*7F

//point D - alarm -  62,68m   from HOME point;  (15.03m from On The Border)
//Latitude:	51.13420629�
//Longitude:	16.97507188�
#define COORD_D {51.134206, 16.975071}
//	$GPGGA,213908.983,5108.052,N,01658.504,E,1,12,1.0,0.0,M,0.0,M,,*6D
//	$GPGSA,A,3,01,02,03,04,05,06,07,08,09,10,11,12,1.0,1.0,1.0*30
//	$GPRMC,213908.983,A,5108.052,N,01658.504,E,029.2,045.6,020920,000.0,W*7E

SCoord testCordsTab [] = {COORD_A,COORD_A,COORD_A,COORD_A,
						COORD_B,COORD_B,COORD_B,COORD_B,
						COORD_B,COORD_B,COORD_C,COORD_C,
						COORD_C,COORD_C,COORD_C,COORD_D,
						COORD_D,COORD_D,COORD_D,COORD_D,
						COORD_D,COORD_C,COORD_B,COORD_A};

//SCord testCordsTab [] = {COORD_B,COORD_B,COORD_B,COORD_B};
//SCord testCordsTab [] = {COORD_D,COORD_D,COORD_D,COORD_D};

uint16_t testCordsTabLengh;
uint16_t testCordsTabIdx = 0;
/****************************/



#endif /* USERMAIN_H_ */
