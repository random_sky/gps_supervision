/*
 * userMain.c
 *
 *  Created on: 23.08.2020
 *      Author: asus
 */

#include "userMain.h"
#include "stm32l4xx_hal.h"
#include "main.h"



uint8_t welcomeScreenTab[] = "\n\r\n\r\n\r HELLO INZYNIERKA 0.2.7 BETA \n\r";
uint8_t initScreenTab[] = "\n\r\n\r\n\r ** INIT STATE ** \n\r";

//****************************************************************************
  void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
  {
  	/*** For GPS ***/
  	/* !!!  IMPORTANT !!!
  	 *
  	 * It is iportant to call this function in  HAL_UART_RxCpltCallbac
  	 * to redirect data to library.
  	 */
	LED3_TOG;
  	GPS_UART_RxCpltCallback(huart);
  	/*** ! ONLY FOR TEST ! ***/
  			HAL_UART_Transmit_DMA(&HUART_GPS,(uint8_t*) "*", 1);// NOT NEEEDED - only for vizualization sended data to STM

  }

//****************************************************************************
  void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin){
	  if(GPIO_Pin == HOME_BUTTON_Pin){
		  HAL_TIM_Base_Start_IT(&htim6);
	  }
  }

//****************************************************************************
  void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim){
	  if(htim->Instance == TIM6){
		  if (HAL_GPIO_ReadPin(HOME_BUTTON_GPIO_Port,HOME_BUTTON_Pin) == GPIO_PIN_RESET){
		  LED3_TOG;
		  buttonEvent = true;
		  }
		  HAL_TIM_Base_Stop(&htim6);
		  htim->Instance->CNT = 0;
	  }
  }

  /****************************************************************************
   *
   *			userMain
   *
   * @brief
   * 	main function
   *
   ****************************************************************************/
  void userMain() {
	  /*** Welcome Screen ***/
		  HAL_UART_Transmit_DMA(&HUART_PC,welcomeScreenTab,sizeof(welcomeScreenTab)/sizeof(welcomeScreenTab[0]));
		  HAL_Delay(2000);

	  /*** INIT ***/
		  machineState = STATE_INIT;

	  /*** GPS ***/
			GPS_Init(&HUART_GPS); // UART which GPS is connected to

			//GPS_sendConfigurationToGps(); // Sending configuration to GPS

			/* For testing sent this message thru terminal (UART1):
			 * $GNRMC,000930.00,A,5100.35365,N,01702.27416,E,1.274,,040219,,,A*6E
			 * $GNGGA,000930.00,4455.35365,N,01702.27416,E,1,04,3.10,129.4,M,40.5,M,,*45
			 * $GNGGA,000930.00,4433.35365,N,01702.27416,E,1,04,3.10,129.4,M,40.5,M,,*45
			 * $GPGGA,183730,3907.356,N,12102.482,W,1,05,1.6,646.4,M,-24.1,M,,*75
			 */

#ifdef FAKE_GPS_TEST
			testCordsTabLengh=sizeof(testCordsTab)/sizeof(testCordsTab[0]);
#endif

	  while(1) {
		  EReturnCode retCode = RETURN_OK;

		  /*** State Machnie ***/
		  switch (machineState) {
			case STATE_INIT://--------------------------------------------------------------------
				//ustawiamy pojemnik na dane z gps'a - zerujemy
			{
				machineState = STATE_HMI;
			  	/*** ! ONLY FOR TEST ! ***/
			  			HAL_UART_Transmit_DMA(&HUART_GPS,(uint8_t*) "#", 1);// NOT NEEEDED - only for vizualization sended data to STM

				//LED Welcome
				LED_GREEN_ON;LED_RED_OFF; HAL_Delay(100); LED_GREEN_OFF;LED_RED_ON;HAL_Delay(100);
				LED_GREEN_ON;LED_RED_OFF; HAL_Delay(100); LED_GREEN_OFF;LED_RED_ON;HAL_Delay(100);

				//LED indicate INIT s
				LED_GREEN_ON;LED_RED_ON;  HAL_Delay(500);

				//Erase lastCordsTab
				lastCordsTab_idx = 0;
				uint8_t i = 0;
				for(i=0; i<AMOUNT_OF_CORDS_TO_MV; i++)
					{
					 lastCordsTab[i].latitude = homeCoordinates.latitude;//TODO: change 0 to come define like DEFOULT_LAT
					 lastCordsTab[i].longitude = homeCoordinates.longitude;
					}

				//Clear button event
				buttonEvent = false;

				//Cancel allarm
				if(cancelAlarm() != RETURN_OK)errorOccurred();

				//print init screen
				HAL_UART_Transmit_DMA(&HUART_PC,initScreenTab,sizeof(initScreenTab)/sizeof(initScreenTab[0]));

				//LED indicate INIT done
				LED_GREEN_OFF;LED_RED_OFF;HAL_Delay(200);
			}break;
			case STATE_HMI://--------------------------------------------------------------------
				machineState = STATE_GPS;

				//if buttonEvent is true, set home coord as cuurent coord and go to STATE_INIT
				if (buttonEvent){
					buttonEvent = false;
					homeCoordinates.latitude = currentCoordinates.latitude;
					homeCoordinates.longitude = currentCoordinates.longitude;
					machineState = STATE_INIT;
				}
				break;
			case STATE_GPS://--------------------------------------------------------------------
				machineState = STATE_CALC;

				//if we have a gps's fix then green led blinking
				if(GPS_get_isFixed()) LED_GREEN_TOG;//TODO: delete invetr("!") in condition
				else LED_GREEN_OFF;

				//get data from GPS
#ifndef FAKE_GPS_TEST
				lastCordsTab[lastCordsTab_idx].latitude = GPS_get_latitude_cord();
				lastCordsTab[lastCordsTab_idx].longitude = GPS_get_longitude_cord();
#else
				lastCordsTab[lastCordsTab_idx].latitude = testCordsTab[testCordsTabIdx].latitude;//insted of GPS_get_latitude_cord();
				lastCordsTab[lastCordsTab_idx].longitude = testCordsTab[testCordsTabIdx].longitude;//insted of GPS_get_longitude_cord();
#endif

				lastCordsTab_idx++;
				if( lastCordsTab_idx > (AMOUNT_OF_CORDS_TO_MV-1) ) lastCordsTab_idx = 0;
#ifdef FAKE_GPS_TEST
				testCordsTabIdx++;
				if( testCordsTabIdx > (testCordsTabLengh-1) ) testCordsTabIdx = 0;
#endif
				break;
			case STATE_CALC://-------------------------------------------------------------------
			{
				machineState = STATE_PRINT;

				//filtering coordinates - Moving Average filter
				SCoord averageOfCords = {0};
				uint8_t i = 0;
				for(i=0; i<AMOUNT_OF_CORDS_TO_MV; i++)
					{
					averageOfCords.latitude += lastCordsTab[i].latitude;
					averageOfCords.longitude += lastCordsTab[i].longitude;
					}
				averageOfCords.latitude /= AMOUNT_OF_CORDS_TO_MV;
				averageOfCords.longitude /= AMOUNT_OF_CORDS_TO_MV;

				//save last coords after filtering to cuurend cords
				currentCoordinates.latitude = averageOfCords.latitude;
				currentCoordinates.longitude = averageOfCords.longitude;

				//calsulate distance from home coordinates
				getDistanceBetweenTwoCoords(&distanceFromHome,&homeCoordinates,&currentCoordinates);
				if(distanceFromHome > MAX_DISTANCE_FROM_HOME) setAlarm();
				else cancelAlarm();

			}break;
			case STATE_PRINT://------------------------------------------------------------------
				machineState = STATE_ALARM;

				//print data to UART(Blooteoth)
				printInfo(&HUART_PC, dataToPrint);

				//main delay before next round
				HAL_Delay(1000);

				break;
			case STATE_ALARM://------------------------------------------------------------------
				machineState = STATE_HMI;
				//NOT USED STATE
				break;
			default://---------------------------------------------------------------------------
				//Error
				break;
		}/*end of swich case*/



		  if(retCode != RETURN_OK) //TODO: add some Error Handler
		  retCode = RETURN_OK;
	  }/*end of while*/
  }/*end of userMain*/


/************************************************************************************/
  EReturnCode printInfo(UART_HandleTypeDef *huart, uint8_t *pData)
  {
	  int size = sprintf((char*)pData,
			  			  		"\n\r\n\r---GPS Supervision---\n\r"
			  			  		"isFixed: %d\n\r"
			  			  		"latitude_float: %f\n\r"
			  			  		"latitude_cord: %f\n\r"
			  			  		"longitude_float: %f\n\r"
			  			  		"longitude_cord: %f\n\r"
			  			  		"speed_float: %f\n\r"
			  			  		"timestamp: %ld\n\r"
								"uartRxPointer: %d\n\r"
								"\n\r"
								"homeCoordinates.latitude: %f\n\r"
								"homeCoordinates.longitude: %f\n\r"
								"currentCoordinates.latitude: %f\n\r"
								"currentCoordinates.longitude: %f\n\r"
								"lastCordsTab[0].latitude: %f\n\r"
								"lastCordsTab[1].latitude: %f\n\r"
								"distanceFromHome: %lf\n\r"
					  	  	  	  ,
			  			  		GPS_get_isFixed(),
			  			  		GPS_get_latitude_float(),
			  			  		GPS_get_latitude_cord(),
			  			  		GPS_get_longitude_float(),
			  			  		GPS_get_longitude_cord(),
			  			  		GPS_get_speed_float(),
			  			  		(long int)GPS_get_timestamp(),
								uartRxMachinery.uartRxPointer,
								homeCoordinates.latitude,
								homeCoordinates.longitude,
								currentCoordinates.latitude,
								currentCoordinates.longitude,
								lastCordsTab[0].latitude,
								lastCordsTab[1].latitude,
								distanceFromHome
								);

			  HAL_UART_Transmit_DMA(huart,(uint8_t*) dataToPrint, size); //TODO: change it to DMA

			  return (size ? RETURN_ERROR : RETURN_OK);
  }
  //****************************************************************************
  EReturnCode getDistanceBetweenTwoCoords(double 	*distInMeter,
		  	  	  	  	  	  	  	  	  SCoord 	*pointA,
										  SCoord 	*pointB)
  {
	  /* NOTE: this method  assuming that the earth is a perfect ball */

	  //defines to quick calc deg to rad
	  #define TO_RADIANS(deg) ((M_PI) / 180 * deg)

	  //convert coords from degree to radians.
	  double lat_A = TO_RADIANS(pointA->latitude);
	  double long_A = TO_RADIANS(pointA->longitude);
	  double lat_B = TO_RADIANS(pointB->latitude);
	  double long_B = TO_RADIANS(pointB->longitude);

	  //Haversine Formula
	  long double div_long = long_B - long_A;
	  long double div_lat = lat_B - lat_A;

	  long double ans = pow(sin(div_lat / 2), 2) +
							cos(lat_A) * cos(lat_B) *
							pow(sin(div_long / 2), 2);

	  ans = 2 * asin(sqrt(ans));

	  //radius of Earth in km, R = 6371
	  long double R = 6371;

	  // Calculate the result and change to meter
	  *distInMeter = ans * R * 1000;

	  return RETURN_OK;
  }

  //****************************************************************************
  EReturnCode setAlarm()
  {
	  isAlarm = true;
	  LED_RED_ON;
	  return RETURN_OK;
  }

  //****************************************************************************
  EReturnCode cancelAlarm()
  {
	  isAlarm = false;
	  LED_RED_OFF;
	  return RETURN_OK;
  }

  //****************************************************************************
void errorOccurred()
  {

  }
